const HabilityRank = {
    junior: Symbol('junior'),
    senior: Symbol('senior'),
    master: Symbol('master')
}

module.exports = HabilityRank;