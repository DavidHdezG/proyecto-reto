const Status = {
    productBacklog: Symbol('productBacklog'),
    sprintBacklog: Symbol('sprintBacklog'),
    releaseBacklog: Symbol('releaseBacklog')
}

module.exports = Status;