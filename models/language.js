const Language = {
    javascript: Symbol('javascript'),
    java: Symbol('java'),
    python: Symbol('python'),
    html: Symbol('html'),
    php: Symbol('php')
}

module.exports = Language;