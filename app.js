var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var projectsRouter = require('./routes/projects');
var teamMembersRouter = require('./routes/teamMembers');
var teamsRouter=require('./routes/teams');
var userStoriesRouter=require('./routes/userStories');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/projects', projectsRouter);
app.use('/teamMembers', teamMembersRouter);
app.use('/teams', teamsRouter);
app.use('/userStories', userStoriesRouter);

module.exports = app;
