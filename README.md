# Proyecto reto

### Integrantes
- Luis Raúl Chacón Muñoz - 339011
- David Eduardo Hernández García -338953

## Estructura del proyecto
![Estructura del proyecto](/diagrams/Estructura.png)

## Diagrama de clases
![clases](/diagrams/ScrumWP.png)

## Rutas de la API
/ - Ruta raíz de la API <br>
/users - Ruta para el manejo de usuarios <br>
/projects - Ruta para el manejo de proyectos <br>
/teamMembers - Ruta para el manejo de miembros de equipo <br>
/teams - Ruta para el manejo de equipos <br>
/userStories - Ruta para el manejo de historias de usuario <br>

## Diagrama de secuencia
![secuencia](/diagrams/SCRUMWP_secuencia.png)

## Link del contenedor en DockerHub
[Enlace al contenedor](https://hub.docker.com/repository/docker/davidehdezg/proyecto-reto/general)

## Link Heroku App
[Enlace a la app](https://proyecto-reto.herokuapp.com)
