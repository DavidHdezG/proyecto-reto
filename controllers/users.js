const express = require("express");

function list(req, res, next) {
  res.send("respond with list");
}

function create(req, res, next) {
  res.send("respond with create");
}

function index(req, res, next) {
    res.send("respond with send");
  }
function replace(req, res, next) {
  res.send("respond with replace");
}

function update(req, res, next) {
  res.send("respond with update");
}

function destroy(req, res, next) {
  res.send("respond with destroy");
}

module.exports = { list, index,create, replace, update, destroy };
